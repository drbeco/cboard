.\" Copyright (C) 2002-2013  Ben Kibbey <bjk@luxsci.net>
.\" 
.\" This program is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 2 of the License, or
.\" (at your option) any later version.
.\" 
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\" 
.\" You should have received a copy of the GNU General Public License
.\" along with this program; if not, write to the Free Software
.\" Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
.de URL
\\$2 \(laURL: \\$1 \(ra\\$3
..
.if \n[.g] .mso www.tmac
.TH CBOARD 6 "Dec 12 2014" "@VERSION@" "Curses Chess Board"

.SH NAME
.B cboard

.SH SYNOPSIS
.B cboard
.I "[\-hvC]" "[-u [0|1]]" "[\-p [\-VtRSE] <filename>]"

.SH DESCRIPTION
.B cboard 
is an
.BR ncurses (3)
PGN browser, editor and frontend to various chess engines supporting the
XBoard protocol.

.SH OPTIONS
The following command line options are available:
.TP
.I "\-p <pgnfile>"
Load a PGN formatted file. If the filename extension ends with .Z, .bz2,
\&.gz or \&.zip, then it will be uncompressed (and compressed if saving)
automatically provided you have
.BR compress (1)
,
.BR bzip2 (1)
,
.BR gzip (1)
and
.BR zip (1)
installed.

.TP
.I "\-E"
Stop processing a file when a parse error occurs. Overrides configuration
parameter.

.TP
.I "\-V"
Validate the specified file only. If a parse error occurs
.B cboard
will return a nonzero exit status.

.TP
.I "\-S"
Validate the specified file and output the PGN formatted game to stdout. If a
parse error occurs 
.B cboard
will return a nonzero exit status.

.TP
.I "\-R"
Like
.I "\-S"
but output a reduced PGN formatted game which includes only the seven tag
roster and move text without any annotations.

.TP
.I "\-t"
Normally when
.I "\-S"
is specified custom tags aren't written. This option enables custom tags in
.B ~/.cboard/config
to be added to the output.

.TP
.I "\-C"
Enable strict castling. Useful when only validating a file. This option
overrides the config value.

.TP
.I "\-u [1|0]"
Enable or disable UTF-8 chess pieces. This will override any
.B utf8_pieces
configuration setting.

.TP
.I "\-v"
Version information.

.TP
.I "\-h"
Program help text.

.SH "CONFIGURATION FILE"
Empty lines and lines beginning with a '#' are ignored. Other lines are
parameter and value pairs which must be on the same line and separated with
one or more space or TAB characters. Options which are noted to accept a value
of
.I bool
can be one of:
.B true
,
.B 1
,
.B on
,
.B yes
OR
.B false
,
.B 0
,
.B off
,
.B no
\&.

.TP
.I utf8_pieces  <bool>
Whether to show UTF-8 chess pieces on the board. The default is on.

.TP
.I board_left <bool>
When off, the chess board will be on the right side of the game status
window. The default is on.

.TP
.I coords_y_left <bool>
Whether to show the board ranks on the left or right side of the chess
board. The default is true.

.TP
.I exit_dialog_box <bool>
Confirm before quitting
.B cboard
\&. The default is on.

.TP
.I engine_cmd_blacktag <bool>
Set the Black tag to the chess engine command used during human/engine
play. The default is on.

.TP
.I engine_cmd <command> [<args>] [...]
The chess engine command to execute with any arguments. The default is
.B gnuchess --xboard
Other engines haven't been tested but any engine that supports the XBoard
protocol should work.

.TP
.I engine_protocol <protocol>
The version of the XBoard protocol to use. At the moment all this
means is whether to send SAN formatted moves (protocol version 2) or
regular FRFR formatted moves. The default is 1.

.TP
.I engine_init <command>
A command to send to the chess engine each time a new game or round is started
or reset. Can be specified more than once.

.TP
.I fm_polyglot <bool>
Send the "go" command to the chess engine during the first move. Fixes
a quirk when using polyglot(1).

.TP
.I bind <mode> <key> <command> [<description>]
This allows you to change the default key bindings for each game mode.
Each game 
.I mode
has it's own set of commands
.I command
with an optional
.I description 
which is shown in the help text. See
.B config.example
included in the archive for available game modes and commands.

.TP
.I cbind <type> <key> <command>
Bind the specified
.I key
to the engine command
.I command .
The bound key will only be available in play mode and will only be sent to the
engine if it doesn't conflict with other play mode keys. The
.I type
argument specifies how a repeat count is treated. When
.B set
, typing a number then pressing the bound 
.I key
will append that number to
.I command
\&. When
.B repeat
, typing a number then pressing
.I key
will send
.I command
the repeat amount of times. If
.B none
, then
.I command
will be sent without modification.

.TP
.I macro <mode> <key> <key_sequence>
A 
.I macro 
behaves like pressing a series of keys automatically without
interaction. The specified
.I key
"presses" the keys specified in
.I key_sequence 
when in 
.I mode .
The 
.I mode
may be
.B play
,
.B history
,
.B edit 
or 
.B any
\&. When 
.B any
, global keys may be accessed.

.TP
.I jump_count <integer>
When viewing move history, you can jump more than one move to the next or
previous move with the UP and DOWN keys. This sets the number of moves to
jump. The default is 5.

.TP
.I tag <name> [<value>]
Custom PGN tags to add to new games. This can also be used to overwrite
default tag values. It can also be used more than once.

.TP
.I line_graphics <bool>
Enable or disable board line graphics. The default is off.

.TP
.I board_details <bool>
When enabled, castling availability, the en passasnt square and
attacking pieces will be shown on the board. This can be toggled with
the `^d' key.

.TP
.I save_directory <path>
If set, the default directory where games will be saved and restored from.
A tilde `~' will be expanded to your home directory.

.TP
.I mpl <integer>
The maximum number of full moves to write per line when saving. If 0,
then write the maximum number of moves that will fit in 80 columns. 

.TP
.I save_prompt <bool>
When saving a game, prompt to edit PGN tag data. Or when saving in history
mode, prompt for additional save commands. The default is on.

.TP
.I delete_prompt <bool>
Prompt before deleting games. The default is on.

.TP
.I stop_on_error <bool>
Stop processing a file when a parsing error occurs. Normally a game or round
will be flagged and the rest of the file will still be parsed.

.TP
.I valid_moves <bool>
When a piece is selected, show valid squares the piece can move to. The
.B color_board_white_moves
and
.B color_board_black_moves
color parameters can specify alternate colors for the valid move squares.
The default is on.

.TP
.I strict_castling <bool>
When enabled, a castling move will be invalid if the opponent can attack a
castling square, beit the King, Rook or the squares between the two. The
default is off.

.TP
.I show_attacks <bool>
When both this option and
.I board_details
is enabled, hilight the opponent pieces that can attack the square under
the cursor.

.TP
.I pattern <filter>
When in the file browser you can filter what is displayed with a
.BR glob (7)
pattern. Directories, including hidden directories, are always
displayed reguardless of the pattern. The default is `*.[Pp][Gg][Nn]*'.

.SH COLORS
Colors and attributes can be modified from the configuration
file. There are up to four values for each parameter. The first two
are the foreground and background colors which must be one of:
.B black
,
.B white
,
.B blue
,
.B green
,
.B yellow
,
.B cyan
,
.B magenta
,
.B red
or
.B -
for the default color.

.P
The third and fourth values are optional and set terminal attributes. The
third sets attributes for color terminals and the fourth for non-color
terminals. Multiple attributes may be assigned by separating them with a
comma. Note that not all terminals support all attributes. The following
attributes are recognized:
.B bold
,
.B reverse
,
.B dim
,
.B standout
,
.B underline
,
.B blink
,
.B invisible
,
.B none
or
.B -
for the default attribute.

.P
Some color parameters have a
.B _window
extension. This will set the default color and attibutes for all
characters of the given parameter set.

.TP
.I color_board_window foreground background [<cattr>] [<attr>]
.TP
.I color_board_selected foreground background [<cattr>] [<attr>]
.TP
.I color_board_cursor foreground background [<cattr>] [<attr>]
.TP
.I color_board_black foreground background [<cattr>] [<attr>]
.TP
.I color_board_white foreground background [<cattr>] [<attr>]
.TP
.I color_board_graphics foreground background [<cattr>] [<attr>]
.TP
.I color_board_coords foreground background [<cattr>] [<attr>]
.TP
.I color_board_white_moves foreground background [<cattr>] [<attr>]
.TP
.I color_board_black_moves foreground background [<cattr>] [<attr>]
.TP
.I color_board_castling foreground background [<cattr>] [<attr>]
.TP
.I color_board_enpassant foreground background [<cattr>] [<attr>]
.TP
.I color_board_attack foreground background [<cattr>] [<attr>]
.TP
.I color_board_prev_move foreground background [<cattr>] [<attr>]
.TP
.I color_status_window foreground background [<cattr>] [<attr>]
.TP
.I color_status_title foreground background [<cattr>] [<attr>]
.TP
.I color_status_border foreground background [<cattr>] [<attr>]
.TP
.I color_status_notify foreground background [<cattr>] [<attr>]
.TP
.I color_status_engine foreground background [<cattr>] [<attr>]
.TP
.I color_tag_window foreground background [<cattr>] [<attr>]
.TP
.I color_tag_title foreground background [<cattr>] [<attr>]
.TP
.I color_tag_border foreground background [<cattr>] [<attr>]
.TP
.I color_history_window foreground background [<cattr>] [<attr>]
.TP
.I color_history_title foreground background [<cattr>] [<attr>]
.TP
.I color_history_border foreground background [<cattr>] [<attr>]
.TP
.I color_message_window foreground background [<cattr>] [<attr>]
.TP
.I color_message_title foreground background [<cattr>] [<attr>]
.TP
.I color_message_border foreground background [<cattr>] [<attr>]
.TP
.I color_message_prompt foreground background [<cattr>] [<attr>]
.TP
.I color_input_window foreground background [<cattr>] [<attr>]
.TP
.I color_input_title foreground background [<cattr>] [<attr>]
.TP
.I color_input_border foreground background [<cattr>] [<attr>]
.TP
.I color_input_prompt foreground background [<cattr>] [<attr>]
.TP
.I color_menu foreground background [<cattr>] [<attr>]
.TP
.I color_menu_selected foreground background [<cattr>] [<attr>]
.TP
.I color_menu_highlight foreground background [<cattr>] [<attr>]
.TP
.I color_menu_graphics foreground background [<cattr>] [<attr>]

.SH RETURN VALUE
.B cboard
will return 0 on success or 1 on failure. Failures are file access errors,
parsing errors or command line usage errors.

.SH FILES
.TP
.I ~/.cboard/config
Configuration file.

.TP
.I ~/.cboard/cc.data
Country codes. Used when editing the "Site" tag.

.TP
.I ~/.cboard/nag.data
Appending to this file is fine for custom NAG types, but lines before
140 are reserved by the PGN standard.

.TP
.I ~/.cboard/perl.pl
If compiled with 
.BR perl (1)
support, a file containing subroutines that
.B CBoard 
can filter games through.

.SH SEE ALSO
.BR gnuchess (6)
,
.BR ncurses (3)
,
.BR glob (3)
,
.BR glob (7)
,
.BR regex (7)

.SH AUTHORS
Ben Kibbey <bjk@luxsci.net>
.P
.URL "http://c-board.sourceforge.net/" "CBoard homepage"
