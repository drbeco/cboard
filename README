CBoard is a Console/Curses PGN file browser, editor and interface to chess
engines supporting the XBoard protocol. It supports SAN move format with
annotations including RAV, NAG, single and multi-line comments along with the
FEN tag which can be edited by arranging pieces on the board. It can also
validate and save to (optionally reduced) PGN format. Multiple games
(human/human, human/engine, engine/engine) can be loaded, created, searched
and edited. You can also customize the key bindings and create macros.

Since there are many variations of the curses library, CBoard requires
NCurses to be installed to make porting to other operating systems
easier. There are known issues with Sun/Solaris and NetBSD, although
CBoard has been tested to work well on OpenBSD, FreeBSD and Linux.

Read INSTALL for generic build and installation details. Or just do:

     ./configure && make && make install
     
After installing, type "cboard -h" for command-line options or just "cboard"
then press F1 for help on in-game command keys.

If you want PERL support you'll need to have libperl installed and pass
--with-libperl to 'configure'. When compiled with PERL support, pressing ^O
will "filter" the current game through a PERL subroutine defined in
~/.cboard/perl.pl. The subroutine is passed a single argument which is a
generated FEN tag of the current game. The subroutine should return a FEN tag
after processing.

The latest version can be found at http://c-board.sourceforge.net/. If you
find a bug or have a patch to fix one or a feature request you can use the
Sourceforge Ticket system at http://sourceforge.net/p/c-board/tickets/ or send
me an email.

There is also a public GIT repository available. Anonymous checkouts can be
done by doing:

    git clone git://repo.or.cz/cboard.git

The gitweb interface can be viewed at http://repo.or.cz/w/cboard.git.

Ben Kibbey <bjk@luxsci.net>
Jabber: bjk@jabber.org
http://c-board.sourceforge.net/
